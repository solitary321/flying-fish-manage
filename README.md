# flying-fish-manage

#### 介绍
此项目为 [**flying-fish-gateway**](https://gitee.com/omsgit/flying-fish-gateway) 分布式飞鱼网关管理项目的配套前端管理界面，采用vue2 + element-ui 2.3.x开发；

#### 软件架构
软件架构说明：node 6+、vue2.x 、 element-ui 2.3.x

#### 安装依赖

1.  cnpm install vue-router
2.  cnpm install axios -S
3.  cnpm install echarts
4.  cnpm install echarts-gl
5.  cnpm install --save babel-polyfill


#### 使用说明

1.  通过IDE加载项目工程
2.  提前安装好nodejs,配置
3.  编译：cnpm run build
3.  运行：cnpm run dev

#### 参与贡献

1.  由于本人非全职前端，因此前端VUE使用根据个人习惯编写，有可能不符合您的项目使用规范，欢迎有兴趣的朋友下载、使用、交流
2.  本项目完全免费开源，可自行修改、编辑、另行发版与使用，不受任何商业限制（保不保留原作者信息，无所谓了）

